const filmsA = require('./films');
require('../css/app.css');

const elements = {
  target: null,
  film: null,
  films: filmsA.filmsArray,
  soreted: null,
};

elements.currentFilms = elements.films;

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  slides[slideIndex-1].style.display = "block";
}

window.plusSlides = function (n) {
  showSlides(slideIndex += n);
}

window.currentSlide = function (n) {
  showSlides(slideIndex = n);
}


window.openPage =function(pageName, elmnt, color) {
    // Hide all elements with class="tabcontent" by default */
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Remove the background color of all tablinks/buttons
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
    }
    // Show the specific tab content
    document.getElementById(pageName).style.display = "block";

    // Add the specific color to the button used to open the tab content
    elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();

var slideIndex = 1;
showSlides(slideIndex);



function fillFilms() {
  const films = document.querySelector('.container');
  console.log(elements.currentFilms);
  const filmCards = createFilmsCards(elements.currentFilms);
  films.appendChild(filmCards);
}

function createFilmsCards(films) {
    const fragment = document.createDocumentFragment();
    console.log(films.length);
    films.forEach(film => {
      const element = {};
      element.filmCard = document.createElement('span');
      element.filmCard.className += 'film-card';
      const popup = document.createElement('a');
      popup.href = '#popup-film-card';
      popup.addEventListener('click', (event) => {
          fillPopup(event.currentTarget, film);
      });
      element.filmCard.appendChild(popup);
      const figure = document.createElement('figure');
      const figcaption = document.createElement('figcaption');
      figcaption.appendChild(document.createTextNode(film.name));
      const img = document.createElement('img');
      img.src = film.image;
      figure.appendChild(img);
      figure.appendChild(figcaption);
      element.filmCard.appendChild(figure)
      popup.appendChild(figure);
      fragment.appendChild(element.filmCard);
    });
    return fragment;
  }
  
  

function fillPopup(target, film) {
    elements.target = target;
    elements.film = film;
    const popupFullname = document.querySelector('.popup-film-card .film-info h1');
    popupFullname.textContent = film.name;
    const ageGender = document.querySelector('.popup-film-card .film-info .age');
    ageGender.textContent = film.age;
    const email = document.querySelector('.popup-film-card .film-info .rating');
    email.textContent = film.rating;
    const phone = document.querySelector('.popup-film-card .film-info .duration');
    phone.textContent = film.duration;
    addImage(film);
    const comment = document.querySelector('.popup-film-card .comment p');
    const commentClass = document.querySelector('.popup-film-card .comment');
    comment.textContent = film.description;
    if (comment.textContent.length === 0) {
      commentClass.style.display = 'none';
    } else {
      commentClass.style.display = 'block';
    }
  }
  
  function addImage(film) {
    const teacherImg = document.querySelector('.popup-film-card figure img');
    teacherImg.src = film.image;
    teacherImg.style.display = 'block';
    
  }


  function searchByParameter(usersArray, param) {
    const foundUsers = [];
    const paramStr = param.toString();
    usersArray.forEach((userItem) => {
      if (userItem.name.toUpperCase().includes(paramStr.toUpperCase())
       || userItem.age.toString() === paramStr) {
        foundUsers.push(userItem);
      } else if (typeof (userItem.note) === 'string') {
        if (userItem.note.toUpperCase().includes(paramStr.toUpperCase())) {
          foundUsers.push(userItem);
        }
      }
    });
    return foundUsers;
  }

const btnSearch = document.querySelector('.topnav .search-container form');
  btnSearch.addEventListener('submit', (e) => {
  e.preventDefault();
  e.stopImmediatePropagation();
  const searchParam = document.querySelector('.topnav .search-container .txt-search').value;
  elements.currentFilms = searchByParameter(elements.films, searchParam);
  setFilters();
  reloadFilms();
  });

  
  
  function reloadFilms() {
    const filmList = document.querySelector('.container');
    while (filmList.lastElementChild) {
      filmList.removeChild(filmList.lastElementChild);
    }
    fillFilms();
  }
  

  function setFilters() {
    let release;
    if (document.querySelectorAll('.topnav .release input')[1].checked) {
      release = true;
    } else if (document.querySelectorAll('.topnav .release input')[2].checked) {
      release = false;
    } else {
      release = '';
    }
    const minRating = document.querySelector('.topnav .min-rating').value;
    const maxRating = document.querySelector('.topnav .max-rating').value;
    let age16 = false;
    if (document.querySelector('.filter .checkbox-16').checked) {
      age16 = true;
    }
    let age18 = false;
    if (document.querySelector('.topnav .checkbox-18').checked) {
      age18 = true;
    }
    elements.currentFilms = filterArray(elements.currentFilms, release, age16, age18, minRating, maxRating);
  }
  
  function filterArray(filmsArray, release, age16, age18, minRating, maxRating) {
    const filteredArray = [];
    filmsArray.forEach((film) => {
      let added = true;
      if (release === true)
        {
          if (film.release < 2020)
          {
            added = false;
          }
        }
      else (release === false)
        {
          if (film.release > 2020)
          {
            added = false;
          }
        }
      if (age18 === true && film.age === "18+") {
        added = false;
      }
      if (age16 === true && (film.age === "16+" || film.age === "18+")) {
        added = false;
      }
      console.log(added);
      if (parseInt(film.rating, 10) < parseInt(minRating, 10)
      || parseInt(film.rating, 10) > parseInt(maxRating, 10)) {
        added = false;
      }
      if (added) {
        filteredArray.push(film);
      }
    });
    return filteredArray;
  }

  function filterFilms() {
    const event = new Event('submit');
    btnSearch.dispatchEvent(event);
    setFilters();
    reloadFilms();
  }


const filterReleaseRadioButton = document.querySelectorAll('.topnav .release input');
filterReleaseRadioButton[0].addEventListener('click', filterFilms);
filterReleaseRadioButton[1].addEventListener('click', filterFilms);
filterReleaseRadioButton[2].addEventListener('click', filterFilms);

const filter16 = document.querySelector('.topnav .checkbox-16');
filter16.addEventListener('change', filterFilms);
const filter18 = document.querySelector('.topnav .checkbox-18');
filter18.addEventListener('change', filterFilms);

const filter_rat = document.querySelector('.topnav .checkbox-rating');
filter_rat.addEventListener('change',  () => {
  elements.currentFilms = sortArray(elements.currentFilms);
  reloadFilms();
});

const filterMinRating = document.querySelector('.topnav .min-rating');
const filterMaxRating = document.querySelector('.topnav .max-rating');
filterMinRating.addEventListener('change', () => {
  if (filterMinRating.value === '') {
    filterMinRating.value = '16';
  }
  filterMaxRating.min = filterMinRating.value;
  filterFilms();
});
filterMaxRating.addEventListener('change', () => {
  if (filterMaxRating.value === '') {
    filterMaxRating.value = '100';
  }
  filterMinRating.max = filterMaxRating.value;
  filterFilms();
});


function sortArray(filmsArray, ) {
  const sortedArray = filmsArray.slice();
      sortedArray.sort((a, b) => {
        if (a['rating'] > b['rating']) {
          return -1;
        }
        if (a['rating'] < b['rating']) {
          return 1;
        }
        return 0;
      });
  return sortedArray;
}

function addTablePages(films) {
  let pages = Math.ceil(films.length / 3);
  if (pages === 0) {
    pages = 1;
  }
  const nav = document.querySelector('.tabcontent nav');
  while (nav.lastElementChild) {
    nav.removeChild(nav.lastElementChild);
  }
  let i = 1;
  while (i <= pages) {
    const page = document.createElement('p');
    page.textContent = i;
    page.addEventListener('click', navigatePage);
    nav.appendChild(page);
    i += 1;
  }
  nav.firstElementChild.className = 'current';
}

function navigatePage(event) {
  const currentPage = event.currentTarget;
  const nav = document.querySelector('.tabcontent nav');
  nav.childNodes.forEach((node) => {
    const page = node;
    page.className = '';
  });
  currentPage.className = 'current';
  const container = document.querySelector('.container');
  let i = container.childElementCount - 1;
  while (i > 1) {
    container.removeChild(container.lastElementChild);
    i -= 1;
  }
  fillFilms();
}


// addTablePages(elements.currentFilms);

  fillFilms();